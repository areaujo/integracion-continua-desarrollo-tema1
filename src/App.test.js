import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('Renderiza sin hacer crash', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Suma de números', () => {
  
  expect((1 + 2) == 3);
  
});
